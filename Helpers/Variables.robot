*** Settings ***
Documentation     A resource file with reusable variables.

*** Variables ***
${browser}               Chrome
${loginUrl}              https://docs.google.com/forms/d/e/1FAIpQLScNsgl6JzGTY5W-moSx1Upl1fTHxqUYss8S-xXBV9aVKMfTaQ/viewform
${streetAddressField}    xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer'][8]//input