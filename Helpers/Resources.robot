*** Settings ***
Library                                                              SeleniumLibrary    run_on_failure=Nothing
Library                                                              XvfbRobot
Resource                                                             Variables.robot
Resource                                                             Internal Resources.robot

*** Keywords ***
#================================================================
#Setups And Teardowns
#================================================================
StartXVFB
    Start Virtual Display    3950           1200    24
    Open Browser             ${loginUrl}    ${browser}

SetupEnvironment
    Run Keyword If    %{XVFB_ENABLED} == True    StartXVFB    ELSE    Open Browser    ${loginUrl}    ${browser}

Close Connection
    Delete All Cookies
    Run Keyword If Test Failed    Capture Page Screenshot

#================================================================
#ElementsOperations
#================================================================

Click On              [Arguments]                      ${element}                ${labelName}=None    ${itemNumber}=None
    ${temp}=          Run keyword and return status    Should Match Regexp       ${element}           ^(xpath=|link)
    # The previous row is added to provide the flexibility to enter a locator, instead of the label name.
    Run Keyword If    '${temp}'== 'True'               Click Element And Wait    ${element}
    ...    ELSE IF    '${element}'=="Radio Button"     Click On                  xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer'][5]//div[@data-value='${labelName}']
    ...    ELSE IF    '${element}'=="Button"           Click On                  xpath=//span[contains(text(), '${labelName}')]
    ...    ELSE       Fail                             ${labelName} is not clicked.

#================================================================
# Calendar
#================================================================

Choose Date in Calendar              [Arguments]           ${specificDate}
    ${calendarLocator}=              Set Variable          xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer'][3]//input
    Wait Until Element Is Visible    ${calendarLocator}
    Input Text                       ${calendarLocator}    ${specificDate}

#================================================================
#Verifys
#================================================================
Verify Element Presence                                [Arguments]                       ${boolean}    ${label}=None    ${fieldType}=None
    ${temp}=          Run keyword and return status    Should Match Regexp               ${label}      ^(xpath=|link=|name=)
    Run Keyword If    '${temp}' == 'True'              Element Should Be Visible         ${label}
    ...    ELSE IF    '${fieldType}' == 'Label'        Verify Label Is Present           ${boolean}    ${label}
    ...    ELSE IF    '${fieldType}' == 'Input'        Verify Input Field Is Present     ${boolean}    ${label}
    ...    ELSE IF    '${fieldType}' == 'Select'       Verify Select Field Is Present    ${boolean}    ${label}
    ...    ELSE       Fail                             ${label} is not visible!

Verify Element Contains                                [Arguments]               ${labelName}    ${expectedValue}    ${fieldType}=None
    ${temp}=          Run keyword and return status    Should Match Regexp       ${labelName}    ^xpath=
    Run Keyword If    '${temp}' == 'True'              Element Should Contain    ${labelName}    ${expectedValue}
    ...    ELSE IF    '${fieldType}' == 'Text'         Element Should Contain    xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer']//input[@aria-label='${labelName}']    ${expectedValue}
    ...    ELSE       Fail                             Check your arguments

Verify Element Does Not Contain                        [Arguments]                   ${labelName}    ${expectedValue}    ${fieldType}=None
    ${temp}=          Run keyword and return status    Should Match Regexp           ${labelName}    ^xpath=
    Run Keyword If    '${temp}' == 'True'              Element Should Not Contain    ${labelName}    ${expectedValue}
    ...    ELSE IF    '${fieldType}' == 'Text'         Element Should Not Contain    xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer']//input[@aria-label='${labelName}']    ${expectedValue}
    ...    ELSE       Fail                             Check your arguments
                
Confirmation Page Should Be Open
    Verify Element Presence    True    xpath=//div[contains(text(),'We have received your registration')]                                  
#================================================================
#Inputs
#================================================================
Input Into Field      [Arguments]                      ${labelOfTheField}       ${value}               ${fieldType}=None
    ${temp}=          Run keyword and return status    Should Match Regexp      ${labelOfTheField}     ^xpath=
    Run Keyword If    '${temp}' == 'True'              Input Text               ${labelOfTheField}     ${value}
    ...    ELSE IF    '${fieldType}'=='Text'           Input Into Text Field    ${labelOfTheField}     ${value}
    ...    ELSE IF    '${fieldType}'=='Select'         Select From Drop Down    ${labelOfTheField}     ${value}
    ...    ELSE       Fail                             ${labelOfTheField} is not filled-in!

