*** Settings ***
Library                                                              SeleniumLibrary    run_on_failure=Nothing
Library                                                              XvfbRobot
Resource                                                             Resources.robot

*** Keywords ***
#*******************************************************************************************
# Element operations helpers
#*******************************************************************************************
Click Element And Wait                                          [Arguments]                                   ${element}
    Wait Until Element Is Visible                               ${element}
    Click Element                                               ${element}

Select From Drop Down                [Arguments]     ${label}    ${optionText}
    # The 2 selects this keyword is used for, are not regular selects, they have to be reworked. That is the reason of the following implementation.
    Click On                         xpath=//div[contains(text(),'${label} ')]/parent::div/parent::div/parent::div/parent::div/parent::div//div[@role='listbox']//div[@class='quantumWizMenuPaperselectOptionList']//span
    ${optionLocator}=                Set Variable    xpath=//div[contains(text(),'${label} ')]/parent::div/parent::div/parent::div/parent::div/parent::div//div[@role='listbox']/div[2]/div[@data-value='${optionText}']
    Wait Until Element Is Visible    ${optionLocator}
    Click On                         ${optionLocator}

#*******************************************************************************************
#Inputs
#*******************************************************************************************
Input Into Text Field     [Arguments]     ${label}    ${value}
    ${locator}=           Set Variable    xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer']//input[@aria-label='${label}']
    Clear Element Text    ${locator}
    Input Text            ${locator}      ${value}
 
#================================================================
#Verify Element Presence helpers
#================================================================

Verify Label Is Present                                    [Arguments]                                   ${boolean}                           ${label}
    ${labelXpath}=    Set Variable    xpath=//div[contains(text(), '${label} ')]
    Run Keyword If    '${boolean}' == 'True'               Element Should Be Visible                     ${labelXpath}
    Run Keyword If    '${boolean}' == 'False'              Element Should Not Be Visible                 ${labelXpath}
    Run Keyword If    '${boolean}' != 'False' and '${boolean}' != 'True'            Fail                 The boolean condition is not defined correctly

Verify Input Field Is Present                                   [Arguments]                                   ${boolean}                           ${label}
    ${inputFieldXpath}=    Set Variable                         xpath=//div[@class='freebirdFormviewerViewNumberedItemContainer']//input[@aria-label='${label}']
    Run Keyword If         '${boolean}' == 'True'               Element Should Be Visible                     ${inputFieldXpath}
    Run Keyword If         '${boolean}' == 'False'              Element Should Not Be Visible                 ${inputFieldXpath}
    Run Keyword If         '${boolean}' != 'False' and '${boolean}' != 'True'            Fail                 The boolean condition is not defined correctly

Verify Select Field Is Present                                  [Arguments]                                   ${boolean}                           ${label}
    ${inputFieldXpath}=    Set Variable                         xpath=//div[contains(text(),'${label} ')]/parent::div/parent::div/parent::div/parent::div/parent::div//div[@role='listbox']//div[@class='quantumWizMenuPaperselectOptionList']//span
    Run Keyword If         '${boolean}' == 'True'               Element Should Be Visible                     ${inputFieldXpath}
    Run Keyword If         '${boolean}' == 'False'              Element Should Not Be Visible                 ${inputFieldXpath}
    Run Keyword If         '${boolean}' != 'False' and '${boolean}' != 'True'            Fail                 The boolean condition is not defined correctly

Verify Select Option Is Present                                   [Arguments]                                   ${boolean}                           ${label}
    ${selectOptionXpath}=    Set Variable                         xpath=//div[contains(text(),'Country ')]/parent::div/parent::div/parent::div/parent::div/parent::div//div[@role='listbox']/div[2]/div[@data-value='${label}']
    Run Keyword If           '${boolean}' == 'True'               Element Should Be Visible                     ${selectOptionXpath}
    Run Keyword If           '${boolean}' == 'False'              Element Should Not Be Visible                 ${selectOptionXpath}
    Run Keyword If           '${boolean}' != 'False' and '${boolean}' != 'True'            Fail                 The boolean condition is not defined correctly
