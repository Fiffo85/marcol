*** Settings ***
Library	               Collections
Library                RequestsLibrary
Library                OperatingSystem    
Library                String
Library                JsonValidator

Test Setup    Create a new session

*** Variables ***

${returnUserId}=    3
${updateUserId}=    10
${deleteUserId}=    26
${emailToUpdate}=    'Jeromy10@yahoo.com'
${nameToUpdate}=    'Philip Manolov'

*** Keywords ***

Create a new session
    Create Session    MS    https://5d67b6b46847d40014f663fc.mockapi.io    verify=True    

*** Test Cases ***
Return all users data
    # Testing if the response comes at all.
    ${resp}=    Get Request    MS   /users
    Should Be Equal As Strings    ${resp.status_code}    200
      
Return correct number of users per the set limit
    # This might be an error in the requirements as getting https://5d67b6b46847d40014f663fc.mockapi.io/users/blogs results in error 404.
    # I will do this test for /users, instead.
    
    # First test case - 1 user per page.
    ${paginationParams}=        Create Dictionary    page=1    limit=1
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    ${userNames}=    Get Elements    ${resp.content}    $..name
    ${length}=    Get Length    ${userNames}
    Should Be Equal As Integers   ${length}    1    
    
    # Second test case - 5 users per page.
    ${paginationParams}=        Create Dictionary    page=1    limit=5
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    ${userNames}=    Get Elements    ${resp.content}    $..name
    ${length}=    Get Length    ${userNames}
    Should Be Equal As Integers   ${length}    5 
    
    # Third test case - 10 users per page.
    ${paginationParams}=        Create Dictionary    page=1    limit=10
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    ${userNames}=    Get Elements    ${resp.content}    $..name
    ${length}=    Get Length    ${userNames}
    Should Be Equal As Integers   ${length}    10
      
Return first user
    # This test will work only if the first 3 users are never deleted.
    
    # First test case - 1st user displayed.
    ${paginationParams}=        Create Dictionary    page=1    limit=1
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    # The following row should work, as the validation of JSONPath shows the expression is correct. But 'None' is returned for the id.
    ${id}=    Get Elements    ${resp.content}    $.id
    Should Be Equal   ${id}    [u'1']    # Unicode cannot be compared with String in python2. Hence, the check is used that way.

Return second user   
    # Second test case - 2nd user displayed.
    ${paginationParams}=        Create Dictionary    page=2    limit=1
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    # The following row should work, as the validation of JSONPath shows the expression is correct. But 'None' is returned for the id.
    ${id}=    Get Elements    ${resp.content}    $.id
    Should Be Equal   ${id}    [u'2']
    
Return third user   
    # Third test case - 3rd user displayed.
    ${paginationParams}=        Create Dictionary    page=3    limit=1
    ${resp}=    Get Request    MS   /users    params=${paginationParams}
    # The following row should work, as the validation of JSONPath shows the expression is correct. But 'None' is returned for the id.
    ${id}=    Get Elements    ${resp.content}    $.id
    Should Be Equal   ${id}    [u'3']
    
Verify sorting by name - TODO
    ${paginationParams}=        Create Dictionary    sortBy=name    order=asc
    ${resp}=    Get Request    MS   /users    params=${paginationParams}    
    # ${data}=    Create Dictionary    key1=name
    ${names}=    Evaluate    ${resp.content}    json
    Log    ${names}
    
Return user by ID
    ${resp}=    Get Request    MS   /users/${returnUserId}
    Run Keyword If     ${resp.content}!="Not found"    
    ...    Log    ${resp.content}    console=yes    # I was not sure how to return the user data. It was not specified.
    ...    ELSE    Fail    "A user with the given ID does not exist!"
    
Create a new user - this should be continued once the sorting is sorted (so that you check for a new ID)
    ${resp}=    Post Request    MS   /users/1
    Log    ${resp.content}    console=yes
    # Add a check when you add an existing ID to the request
    
Update a user's email
    ${data}=    Create Dictionary    email=${emailToUpdate}
    ${resp}=    Put Request    MS   /users/${updateUserId}     data=${data}
    ${email}=    Get Elements    ${resp.content}    $.email
    Should Be Equal As Strings    ${emailToUpdate}    ${email}
    # The email fails to update. The Unicode format here is also a problem with python2, as well. 
    
Update a user's name
    ${data}=    Create Dictionary    email=${nameToUpdate}
    ${resp}=    Put Request    MS   /users/${updateUserId}     data=${data}
    ${name}=    Get Elements    ${resp.content}    $.name
    Should Be Equal As Strings    ${nameToUpdate}    ${name}
    # The name update also fails. This probably means that update is forbidden.
    
Delete a user
    ${resp1}=    Delete Request    MS   /users/${deleteUserId}
    ${resp2}=    Run Keyword If     ${resp1.content}!="Not found"    Get Request    MS   /users/${deleteUserId}
    ...    ELSE    Fail    "There is no user with the given ID."
    