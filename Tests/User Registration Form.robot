*** Settings ***
Library                                        SeleniumLibrary          run_on_failure=Nothing
Library                                        XvfbRobot
Resource                                       ../Helpers/Resources.robot

Suite Setup                                    SetupEnvironment
Test Teardown                                  Close Connection
Suite Teardown                                 Close Browser

*** Test Cases ***
Fill Registration Form with Valid Data
    Input Into Field           Name                     Philip            Text
    Input Into Field           Email                    test@test.bg      Text
    Choose Date in Calendar    14.11.1985
    Input Into Field           Phone Number             089596            Text
    Click On                   Radio Button             Male
    Input Into Field           Country                  Bulgaria          Select
    Sleep    1    # This needs to be at least a second, otherwise the robot clicks too fast and messes it up.
    Input Into Field           City                     Sofia             Select
    Sleep    1    # This needs to be at least a second, otherwise the robot clicks too fast and messes it up.
    Input Into Field           ${streetAddressField}    Bla-bla 15
    Click On                   Button                   Изпращане         # My default Chrome is in Bulgarian
    Confirmation Page Should Be Open





